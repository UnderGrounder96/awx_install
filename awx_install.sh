#!/usr/bin/env bash

set -euo pipefail

AWX_VERSION=$(curl -sH "Accept: application/vnd.github.v3+json" https://api.github.com/repos/ansible/awx/releases/latest | grep -Eom 1 "[0-9]{2,3}\.[0-9]{1,2}\.[0-9]{1,2}")

function _logger_info(){
    echo -e "\n[$(date '+%d/%b/%Y %H:%M:%S')]: $*...\n"
}

function prepare_tmp_dir(){
    TMP_DIR=$(mktemp -d)
    ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

    _logger_info "Preparing tmp dir"
    export ROOT_DIR TMP_DIR
    pushd "$TMP_DIR"
}

function clean_tmp_dir(){
    _logger_info "Cleaning tmp dir"
    popd
    # sudo rm -rf "$TMP_DIR"
}

trap clean_tmp_dir EXIT


function installation_requirements(){
    _logger_info "Installing required packages"
    sudo dnf install -y epel-release dnf-utils
    sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    sudo dnf install -y docker-ce conntrack-tools
    sudo sh -c "systemctl enable docker && systemctl start docker"

    # kubectl
    curl -o /bin/kubectl \
    -L "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
    chmod +x /bin/kubectl

    echo "source <(kubectl completion bash)" >> ~/.bashrc # add autocomplete permanently to bash shell.
    echo 'export PATH="$PATH:$HOME/.local/bin:$HOME/bin:/usr/local/bin"' >> ~/.bash_profile
    export PATH="$PATH:$HOME/.local/bin:$HOME/bin:/usr/local/bin"

    # Helm
    # curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash

    # Minikube - latest has ingress problem
    curl -L https://storage.googleapis.com/minikube/releases/latest/minikube-1.18.1-0.x86_64.rpm -o minikube.rpm
    sudo rpm -ivh minikube.rpm
}


function main(){
    prepare_tmp_dir
    installation_requirements

    _logger_info "Starting Minikube"
    minikube start --driver=none --addons=ingress

    _logger_info "Installing AWX ${AWX_VERSION}"
    kubectl apply -f https://raw.githubusercontent.com/ansible/awx-operator/0.8.0/deploy/awx-operator.yaml
    kubectl apply -f $ROOT_DIR/deploy.yml

    _logger_info "Sleeping for 10min"
    sleep 10m # due to k8s slow start and migrations

    _logger_info "Installation logging"
    kubectl get deployment
    kubectl get pods
    kubectl get svc

    _logger_info "Your AWX ADDRESS is: http://localhost:$(minikube service awx-service --url | awk -F: '{print $3}')"
    echo -e "Your AWX password is: $(kubectl get secret awx-admin-password -o jsonpath='{.data.password}' | base64 --decode)\n"

    # _logger_info "Showcasing kubectl logs"
    # kubectl logs --tail 15 $(kubectl get pods | grep awx-operator | awk '{print $1}') # --follow

    exit 0
}

main