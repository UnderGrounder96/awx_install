# AWX_install

This small bash script installs AWX and all of its required packages on EL CentOS 8.

[AWX](https://github.com/ansible/awx) is cloned from the official repo.

## Required packages

Some of the required packages are:

- minikube
- docker-ce
- kubernetes

## Deployment

The script is fool-proof to a degree. It should help you avoid the most basic errors.<br />
Usage:

```bash
sudo bash awx_install.sh
```

Once the installation is complete, please visit: [http://localhost](http://localhost)

Alternatively, if Vagrant and Virtualbox are installed: `vagrant up`. <br />
Using Vagrant, AWX is deployed on host's: `localhost:PORT`

_PORT is a value between: 30000 and 32767_
